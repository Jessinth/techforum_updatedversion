import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {  IDropdownSettings } from 'ng-multiselect-dropdown';
// import custom validator to validate that password and confirm password fields match
import { MustMatch } from '../../services/must-match.validator';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
;
dropdownList = [];
  skills = String;
  dropdownSettings = {};
registerForm: FormGroup;
    submitted = false;
  name: String;
  username: String;
  password: String;
  email: String;
  confirmPassword: String;
  emailRegex = /([a-zA-Z0-9]+)([\.{1}])?([a-zA-Z0-9]+)\@prodapt([\.])com/;
  constructor(
    private validateService: ValidateService,
    private flashMessagesService: FlashMessagesService,
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder,
  ) { }
  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.required],
      skills: ['', Validators.required],
      username: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern('^([a-zA-Z0-9]+)([\.{1}])?([a-zA-Z0-9]+)\@prodapt([\.])com')]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
  }, {
      validator: MustMatch('password', 'confirmPassword')
  });



  this.dropdownList = [
    { item_id: 1, item_text: 'Java' },
    { item_id: 2, item_text: 'Python' },
    { item_id: 3, item_text: 'Angular' },
    { item_id: 4, item_text: '.NET' },
    { item_id: 5, item_text: 'React' }
  ];
  // this.skills = [
  //   { item_id: 3, item_text: 'Angular' },
  //   { item_id: 4, item_text: '.NET' }
  // ];
  this.dropdownSettings = {
    singleSelection: false,
    idField: 'item_id',
    textField: 'item_text',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };


  }


  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }

    // convenience getter for easy access to form fields
    get f() { return this.registerForm.controls; }

  submitRegisterForm() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
        return;
    }

    const user = {
      name: this.name,
      username: this.username,
      password: this.password,
      email: this.email,
      skills: this.skills,

    }

    // Validate required fields
    for (let key in user) {
      if (!user[key]) {
      this.flashMessagesService.show(`Please fill all fields marked in red`, { cssClass: 'alert-danger', timeout: 1500  });
      return false;
      }
    }
    // Validate Email
    if (!this.validateService.validateEmail(this.email)) {
      this.flashMessagesService.show('Enter a valid Prodapt Mail ID', { cssClass: 'alert-danger', timeout: 1500 });
      return false;
    }

    // Register User
    this.authService.registerUser(user).subscribe(
      data => {
        if (data.success) {
          this.flashMessagesService.show(data.msg, { cssClass: 'alert-success', timeout: 1500 });
          this.router.navigate(['/login']);
        } else {
          this.flashMessagesService.show(data.msg, { cssClass: 'alert-danger', timeout: 1500 });
          this.router.navigate(['/register']);
        }
      },
      err => {
        this.authService.handleError(err);
      });
  }

}
